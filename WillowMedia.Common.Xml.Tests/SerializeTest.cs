using System.Globalization;
using System.Runtime.Serialization;
using System.Threading;
using Xunit;

namespace WillowMedia.Common.Xml.Tests;

public class SerializeTest
{
    [Fact]
    public void InvariantTests()
    {
        var nlNL = new CultureInfo("nl-NL");
        Thread.CurrentThread.CurrentCulture = nlNL;
        Thread.CurrentThread.CurrentUICulture = nlNL;
            
        var myObject = new SerializeMeRoot();
        myObject.ANumber = 123.45f;
            
        var xmlHelper = new XmlHelper();
        xmlHelper.Indenting = false;
        xmlHelper.PrettyOutput = false;
        var xml = xmlHelper.Serialize(myObject);

        Assert.NotNull(xml);
        Assert.Equal(
            "<SerializeMeRoot ANumber=\"123.45\"/>", 
            xml);
    }
        
    [Fact]
    public void SerializeValueAttributeTests()
    {
        var myObject = new SerializeMeRoot();
        myObject.MyLowercaseValue = "UPPERCASE";
            
        var xmlHelper = new XmlHelper();
        xmlHelper.Indenting = false;
        xmlHelper.PrettyOutput = false;
        var xml = xmlHelper.Serialize(myObject);

        Assert.NotNull(xml);
        Assert.Equal("<SerializeMeRoot MyLowercaseValue=\"UPPERCASE\"/>", xml);
    }
        
    /// <summary>
    /// Test if the ignoreProperty method validates if property has ignoreDataMember or serializerIgnoreGetter
    /// attributes
    /// </summary>
    [Fact]
    public void IgnoreAttributeTest()
    {
        var type = typeof(ObjectWithIgnoreProperty);
        var xmlHelper = new XmlHelper();

        var ignoreGetter = type.GetProperty("MyValueIgnoreGetter");
        Assert.NotNull(ignoreGetter);
        Assert.True(Serializer.IgnoreForSerialization(ignoreGetter));
            
        var ignoreDataMember = type.GetProperty("MyValueIgnoreDataMember");
        Assert.NotNull(ignoreDataMember);
        Assert.True(Serializer.IgnoreForSerialization(ignoreDataMember));
            
        var notIgnored = type.GetProperty("NotIgnored");
        Assert.NotNull(notIgnored);
        Assert.False(Serializer.IgnoreForSerialization(notIgnored));
    }
        
    [Fact]
    public void RootElementNameTest()
    {
        var xmlHelper = new XmlHelper()
        {
            Indenting = false,
            PrettyOutput = false
        };
            
        var model = new MyRootObject()
        {
            SomeValue = "This is myRoot"
        };

        var serialized = xmlHelper.Serialize(model);
            
        Assert.NotNull(serialized);
        Assert.Equal("<Rooted><SomeValue>This is myRoot</SomeValue></Rooted>", serialized);
    }

    [Fact]
    public void SerializePreferTests()
    {
        var xmlHelper = new XmlHelper();
        var model = new PreferedSerializeModel()
        {
            root = new SerializeMeRoot()
            {
                ANumber = 1.234f,
                MyLowercaseValue = "string"
            }
        };
        var xml = xmlHelper.Serialize(model);
        Assert.Equal("<PreferedSerializeModel>\n\t<root MyLowercaseValue=\"string\" ANumber=\"1.234\"/>\n</PreferedSerializeModel>\n", xml);

        xmlHelper.SerializerPreferAsElement = true;
        xml = xmlHelper.Serialize(model);
        Assert.Equal("<PreferedSerializeModel>\n\t<root>\n\t\t<MyLowercaseValue>string</MyLowercaseValue>\n\t\t<ANumber>1.234</ANumber>\n\t</root>\n</PreferedSerializeModel>\n", xml);
    }

    [Fact]
    public void SerializeInlineXmlTests()
    {
        var model = new RootForInline()
        {
            NL = "Dit is een <say-as>tekst</say-as>"
        };

        var xmlHelper = new XmlHelper()
        {
            PrettyOutput = false,
            Indenting = false
        };
        var xml = xmlHelper.Serialize(model);
        
        Assert.Equal("<RootForInline><NL>Dit is een <say-as>tekst</say-as></NL></RootForInline>", xml);
    }
}

public class RootForInline
{
    [Serializer(Serializer = XmlSerializing.Element, Literal = true)]
    public string NL { get; set; }
}


public class PreferedSerializeModel
{
    public SerializeMeRoot root { get; set; }
}

[SerializerName("Rooted")]
public class MyRootObject
{
    [Serializer(Serializer = XmlSerializing.Element)]
    public string SomeValue { get; set; }
}

public class ObjectWithIgnoreProperty
{
    [SerializerIgnoreGetter]
    public string MyValueIgnoreGetter { get; set; }

    [IgnoreDataMember]
    public string MyValueIgnoreDataMember { get; set; }
        
    public string NotIgnored { get; set; }
}

public class SerializeMeRoot
{
    [SerializeLowercase]
    public string MyLowercaseValue { get; set; }

    public float? ANumber { get; set; }
}