﻿using System.Xml.Serialization;

namespace WillowMedia.Common.Xml.Tests.Reseller;

public class Company
{
    [XmlElement("INSCHRIJVING")]
    public INSCHRIJVING Inschrijving { get; set; }
}