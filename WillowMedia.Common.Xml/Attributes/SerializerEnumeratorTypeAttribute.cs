using System;

namespace WillowMedia.Common.Xml;

[AttributeUsage(AttributeTargets.Property)]
public class SerializerEnumeratorTypeAttribute : Attribute
{
    public Type Type { get; set; }
}