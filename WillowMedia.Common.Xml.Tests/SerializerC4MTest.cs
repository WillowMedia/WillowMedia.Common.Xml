﻿using System.Collections.Generic;
using System.Xml.Serialization;
using Xunit;

namespace WillowMedia.Common.Xml.Tests;

public class SerializerC4MTest
{
    [Fact]
    public void SerializeToxmlTest()
    {
        var c4me = new Configure4Me()
        {
            SortingID = 0,
            IsModulePart = false,
            Version = "1.0.0"
        };
        c4me.ControlDesignObjects = new ControlDesignObjects();
        c4me.ControlDesignObjects.LstObjectIdentifiers = new List<C4M<C4mObjectIdentifier>>();
        c4me.ControlDesignObjects.LstObjectIdentifiers.Add(new C4M<C4mObjectIdentifier>()
        {
            ObjectType = "Airnozzle",
            ObjectIdentifier = "M"
        });
        c4me.ControlDesignObjects.LstObjectIdentifiers.Add(new C4M<C4mObjectIdentifier>()
        {
            ObjectType = "Controlvalve",
            ObjectIdentifier = "V"
        });

        XmlHelper xmlHelper = new XmlHelper()
        {
            CaseSensitivePropertyNameToElement = false
        };

        // Serialize to xml
        var result = xmlHelper.Serialize(c4me);
        Assert.NotNull(result);

        // Deserialize xml to objects
        var result2 = xmlHelper.Deserialize<Configure4Me>(result);
        Assert.NotNull(result2);
        Assert.False(result2.IsModulePart);
        Assert.Equal(0, result2.SortingID);    
        Assert.Equal("1.0.0", result2.Version);
        Assert.Equal(2, result2.ControlDesignObjects.LstObjectIdentifiers.Count);
    }

    [Fact]
    public void ValidateEncoding()
    {
        var c4me = new Configure4Me()
        {
            Version = "< > \' \" &"
        };
            
        XmlHelper xmlHelper = new XmlHelper();
        var result = xmlHelper.Serialize(c4me);
        Assert.NotNull(result);
        var contains = result.Contains("&lt; &gt; &#39; &quot; &amp;");
        Assert.True(contains);
    }
}


     
public class Configure4Me
{
    public int? SortingID { get; set; }
    public bool? IsModulePart { get; set; }
    public string Version { get; set; }
        
    public Projectinformation Projectinformation { get; set; }
    public ControlDesignObjects ControlDesignObjects { get; set; }
}

public class Projectinformation
{
    public int? SortingID { get; set; }
    public bool? IsModulePart { get; set; }
        
    [XmlElement("projectproperties")]
    public ProjectProperties ProjectProperties { get; set; }
}

public class ProjectProperties
{
    public string Description { get; set; }
    public string Remark { get; set; }
    public int? SortingID { get; set; }
    public bool? IsModulePart { get; set; }
    public string Projecttype { get; set; }
    public string Projectnumber { get; set; }
    public string Customer { get; set; }
    public string CustomerReference { get; set; }
    public string ProductgroupLevel01 { get; set; }
    public string ProductgroupLevel02 { get; set; }
    public string ProductgroupLevel03 { get; set; }
    public string ProductgroupLevel04 { get; set; }
    public string ProductgroupLevel05 { get; set; }
    public string ProductgroupLevel06 { get; set; }
    public string ProductgroupLevel07 { get; set; }
    public string ProductgroupLevel08 { get; set; }
    public string ProductgroupLevel09 { get; set; }
    public string ProductgroupLevel10 { get; set; }
    public string EplanMacroFile { get; set; }
}

public class ControlDesignObjects
{
    public int? SortingID { get; set; }
    public bool? IsModulePart { get; set; }
        
//        public List<C4M_ObjectIdentifier> LstObjectIdentifiers { get; set; }
    public List<C4M<C4mObjectIdentifier>> LstObjectIdentifiers { get; set; }
}
    
[SerializerName("C4M")]
public class C4M<T> where T : iC4m
{
    public string ObjectType { get; set; }
    public string ObjectIdentifier { get; set; }
}

public interface iC4m
{
}
    
public class C4mObjectIdentifier : iC4m
{
}