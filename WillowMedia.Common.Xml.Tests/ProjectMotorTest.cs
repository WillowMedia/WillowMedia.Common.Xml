using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Xunit;

namespace WillowMedia.Common.Xml.Tests.ProjectMotor;

public class ProjectMotorTest
{
    [Fact]
    public void DeserializeProjectMotorTest()
    {
        var assembly = this.GetType().Assembly;
        var resourceName = assembly.GetManifestResourceNames().FirstOrDefault(r => r.EndsWith("ProjectMotorTest-test.xml"));

        Assert.NotNull(resourceName);

        using (var stream = assembly.GetManifestResourceStream(resourceName))
        using (StreamReader reader = new StreamReader(stream))
        {
            string xml = reader.ReadToEnd();

            Assert.True(!String.IsNullOrEmpty(xml));

            var xmlHelper = new XmlHelper();
            var c4m = xmlHelper.Deserialize<Configure4Me>(xml);

            Assert.NotNull(c4m);
            Assert.NotNull(c4m.controldesignObjects.C4M_Motors);
            Assert.Equal(4, c4m.controldesignObjects.C4M_Motors.Count);
        }
    }
}

//public class XmlHelperMotor : XmlHelper 
//{
//    public override void ParseList(
//        PropertyInfo property, 
//        IEnumerable<XElement> children, 
//        object ienumerator) 
//    {
//        // Get child type
//        var childType = property.GetCustomAttribute<XmlArrayItemAttribute>()?.Type ??
//            property.PropertyType.GetGenericArguments().FirstOrDefault();

//        // int count = 0;

//        foreach (var childElement in children) {
//            (ienumerator as IList)?.Add(ParseType(childType, childElement));
//        }

//        //Assert.Equal(4, count);
//        //Console.WriteLine($"Child count={count}");
//    }
//}

public class Configure4Me 
{
    public string sortingId { get; set; }
    public string isModulePart { get; set; }
    public string Version { get; set; }
    public controldesignObjects controldesignObjects { get; set; }
}

public class controldesignObjects 
{
    public string sortingId { get; set; }
    public string isModulePart { get; set; }
        
    public List<C4M_Grouping> C4M_Groupings { get; set; }
    public List<C4M_Motor> C4M_Motors { get; set; }
}

public class C4M_Grouping 
{ 
    public string ID { get; set; }
}

public class C4M_Motor 
{
    public string ID { get; set; }
}