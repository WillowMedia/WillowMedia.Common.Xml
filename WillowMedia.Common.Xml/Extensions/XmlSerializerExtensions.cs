using System.IO;

namespace System.Xml.Serialization;

public static class XmlSerializerExtensions
{
    public static object Deserialize(this XmlSerializer serializer, string xml)
    {
        using (StringReader stringReader = new StringReader(xml))
            return serializer.Deserialize(stringReader);
    }
}