using System;

namespace WillowMedia.Common.Xml;

public class PropertyElementInfo
{
    public string Name { get; set; }
    public Type PropertyType { get; set; }
    public Type OverrideType { get; set; }
}