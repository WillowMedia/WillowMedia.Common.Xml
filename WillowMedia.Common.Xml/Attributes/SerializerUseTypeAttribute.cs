using System;

namespace WillowMedia.Common.Xml;

[AttributeUsage(AttributeTargets.Property)]
public class SerializerUseTypeAttribute : Attribute
{
    public Type DefaultType { get; set; }
        
    /// <summary>
    /// If type is missing on deserialize, don't throw an exception, just ignore the element
    /// </summary>
    public bool IgnoreUndefined { get; set; }
}