using System.Collections.Generic;
using System.Globalization;
using Xunit;

namespace WillowMedia.Common.Xml.Tests;

public class SerializeStylingTests
{
    [Fact]
    public void SerializePrettyOutputWithAttributesTest()
    {
        var root = new RootNode();
        root.Nodes = new List<Node>()
        {
            new Node()
            {
                Label = "node 1",
                Nodes = new List<Node>()
                {
                    new Node()
                    {
                        Label = "node 1.1",
                        Questions = new List<Question>()
                        {
                            new Question()
                            {
                                Content = "some question\nwith additional lines"
                            }
                        }
                    }
                }
            }
        };

        var xmlHelper = new XmlHelper()
        {
            Indenting = true,
            PrettyOutput = true,
            Culture = CultureInfo.InvariantCulture
        };
        var xml = xmlHelper.Serialize(root);
        var expected = "<RootNode>\n" +
                       "	<Node Label=\"node 1\">\n" +
                       "		<Node Label=\"node 1.1\">\n" +
                       "			<Question>\n" +
                       "				<Content>some question\n" +
                       "with additional lines</Content>\n" +
                       "			</Question>\n" +
                       "		</Node>\n" +
                       "	</Node>\n" +
                       "</RootNode>\n";

        Assert.Equal(expected, xml);
    }

    [Fact]
    public void SerializePrettyOutputTest()
    {
        var root = new RootNode();
        root.Nodes = new List<Node>()
        {
            new Node()
            {
                Label = "node 1",
                Nodes = new List<Node>()
                {
                    new Node()
                    {
                        Label = "node 1.1",
                        Questions = new List<Question>()
                        {
                            new Question()
                            {
                                Content = "some question\nwith additional lines"
                            }
                        }
                    }
                }
            }
        };

        var xmlHelper = new XmlHelper()
        {
            Indenting = true,
            PrettyOutput = true,
            Culture = CultureInfo.InvariantCulture
        };
        var xml = xmlHelper.Serialize(root);
        var expected = "<RootNode>\n" +
                       "	<Node Label=\"node 1\">\n" +
                       "		<Node Label=\"node 1.1\">\n" +
                       "			<Question>\n" +
                       "				<Content>some question\n" +
                       "with additional lines</Content>\n" +
                       "			</Question>\n" +
                       "		</Node>\n" +
                       "	</Node>\n" +
                       "</RootNode>\n";

        Assert.Equal(expected, xml);
    }

    [Fact]
    public void SerializeWithoutPrettyOutputTest()
    {
        var root = new RootNode();
        root.Nodes = new List<Node>()
        {
            new Node()
            {
                Label = "node 1",
                Nodes = new List<Node>()
                {
                    new Node()
                    {
                        Label = "node 1.1",
                        Questions = new List<Question>()
                        {
                            new Question()
                            {
                                Content = "some question\nwith additional lines"
                            }
                        }
                    }
                }
            }
        };

        var xmlHelper = new XmlHelper()
        {
            Indenting = false,
            PrettyOutput = false,
            Culture = CultureInfo.InvariantCulture
        };
        var xml = xmlHelper.Serialize(root);
        // var expected = "<RootNode><Node><Label>node 1</Label><Node><Label>node 1.1</Label><Question><Content>some question\nwith additional lines</Content></Question></Node></Node></RootNode>";
        var expected =
            "<RootNode><Node Label=\"node 1\"><Node Label=\"node 1.1\"><Question><Content>some question\nwith additional lines</Content></Question></Node></Node></RootNode>";

        Assert.Equal(expected, xml);
    }

    public class RootNode
    {
        [Serializer(Inline = true)]
        public List<Node> Nodes { get; set; }
    }

    public class Node
    {
        public string Label { get; set; }

        [Serializer(Inline = true)]
        public List<Node> Nodes { get; set; }

        [Serializer(Inline = true)]
        public List<Question> Questions { get; set; }
    }

    public class Question
    {
        public string Content { get; set; }
    }
}