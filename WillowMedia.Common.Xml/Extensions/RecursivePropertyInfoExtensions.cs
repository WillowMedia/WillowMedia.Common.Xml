﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace WillowMedia.Common.Xml;

public static class RecursivePropertyInfoExtensions
{
    public static List<PropertyInfo> GetAllProperties(this Type type)
    {
        return GetAllProperties(type, new Dictionary<string, PropertyInfo>()).Select(d => d.Value).ToList();
    }

    private static Dictionary<string, PropertyInfo> GetAllProperties(this Type type, Dictionary<string, PropertyInfo> found)
    {
        var properties = type.GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);

        foreach (var p in properties)
            if (!found.ContainsKey(p.Name))
                found.Add(p.Name, p);

        if (type.BaseType != null)
            return GetAllProperties(type.BaseType, found);

        return found;
    }
}