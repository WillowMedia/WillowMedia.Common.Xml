﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Xml.Serialization;

namespace WillowMedia.Common.Xml;

public static class PropertyInfoExtensions
{
    public static string GetNonGenericName(this Type type)
    {
        var name = type.Name;
        int index;
        if ((index = name.IndexOf('`')) > -1)
            name = name.Substring(0, index);

        return name;
    }
        
    public static PropertyElementInfo ElementInfo(this PropertyInfo property)
    {
        var xmlElementAttr = property.GetCustomAttribute<XmlElementAttribute>();

        var elementName = 
            xmlElementAttr?.ElementName ??
            property.Name;

        var overrideType =
            property.GetCustomAttribute<SerializerTypeAttribute>()?.Type ??
            xmlElementAttr?.Type;

        var propertyType = property.PropertyType;

        return new PropertyElementInfo()
        {
            Name = elementName,
            PropertyType = propertyType,
            OverrideType = overrideType
        };
    }
        
    public static PropertyElementInfo ListElementInfo(this PropertyInfo property)
    {
        var xmlElementAttr = property.GetCustomAttribute<XmlElementAttribute>();
        var propertyType = property.PropertyType;
        var isEnumerator = propertyType.IsEnumerable();
            
        if (isEnumerator)
        {
            if (propertyType.IsGenericType &&
                propertyType.GetGenericTypeDefinition() == typeof(List<>))
            {
                propertyType = propertyType.GetGenericArguments()[0]; 
            }
            else if (propertyType.BaseType.IsGenericType &&
                     propertyType.BaseType.GetGenericTypeDefinition() == typeof(List<>))
            {
                propertyType = propertyType.BaseType.GetGenericArguments()[0]; 
            }
        }
            
        // If property is list, then get the name of T
        var elementName = 
            xmlElementAttr?.ElementName ??
            (isEnumerator ? propertyType.GetNonGenericName() : null) ??
            property.Name;
            
        var overrideType =
            property.GetCustomAttribute<SerializerEnumeratorTypeAttribute>()?.Type ??
            xmlElementAttr?.Type;

        return new PropertyElementInfo()
        {
            Name = elementName,
            PropertyType = propertyType,
            OverrideType = overrideType,
        };
    }
        
    public static bool IsEnumerable(this Type type) 
    {
        return 
            Type.GetTypeCode(type) == TypeCode.Object &&
            typeof(IEnumerable).IsAssignableFrom(type);
    }
}