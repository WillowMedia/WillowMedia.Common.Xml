﻿namespace WillowMedia.Common.Xml.Tests.Reseller;

public class WERKGEVER
{
    public int? WERKGEVER_ID { get; set; }
    public string WERKGEVER_NAAM { get; set; }
    public string WERKGEVER_TELEFOONNR { get; set; }
    public string WERKGEVER_ADRES { get; set; }
    public string WERKGEVER_POSTCODE { get; set; }
    public string WERKGEVER_PLAATS { get; set; }
    public string WERKGEVER_LAND { get; set; }
    public string WERKGEVER_POSTADRES { get; set; }
    public string WERKGEVER_POSTPOSTCODE { get; set; }
    public string WERKGEVER_POSTPLAATS { get; set; }
    public string WERKGEVER_POSTLAND { get; set; }
    public string WERKGEVER_FACTADRES { get; set; }
    public string WERKGEVER_FACTPOSTCODE { get; set; }
    public string WERKGEVER_FACTPLAATS { get; set; }
    public string WERKGEVER_FACTLAND_CODE { get; set; }
}