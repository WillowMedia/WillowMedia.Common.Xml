﻿using System.IO;
using System.Linq;
using WillowMedia.Common.Xml.Tests.Reseller;
using Xunit;

namespace WillowMedia.Common.Xml.Tests;

public class ResellerTests
{
    [Fact]
    public void DeserializeTest()
    {
        var assembly = this.GetType().Assembly;
        var resourceName = assembly.GetManifestResourceNames().FirstOrDefault(r => r.EndsWith("Reseller.xml"));

        Assert.NotNull(resourceName);

        using (var stream = assembly.GetManifestResourceStream(resourceName))
        using (var reader = new StreamReader(stream))
        {
            string xml = reader.ReadToEnd();
            var xmlHelper = new XmlHelper();
            var company = xmlHelper.Deserialize<Company>(xml);

            Assert.NotNull(company);
            Assert.NotNull(company.Inschrijving);
            Assert.NotNull(company.Inschrijving.CURSIST);
            Assert.NotNull(company.Inschrijving.CURSIST.CURSIST_ID);
            Assert.Equal(12345, company.Inschrijving.CURSIST.CURSIST_ID);
            Assert.Null(company.Inschrijving.WERKGEVER.WERKGEVER_ID);
        }
    }
}