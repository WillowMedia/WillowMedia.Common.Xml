﻿using System;
using System.Reflection;
using System.Runtime.Serialization;
using System.Xml;

namespace WillowMedia.Common.Xml;

public static class XmlNodeExtension
{
    /// <summary>
    /// Deserializes a node to an object, with using the datamember attribute for referencing to the correct child node
    /// </summary>
    public static T Deserialize<T>(this XmlNode node) where T : new()
    {
        T model = new T();

        foreach (XmlNode childNode in node)
        {
            // Check all the properties for a datamember with the same name
            DataMemberAttribute datamember = null;
                
            foreach (var property in typeof(T).GetProperties())
            {
                // If datamember matches the node name
                if ((datamember = property.GetCustomAttribute<DataMemberAttribute>()) != null &&
                    string.Equals(datamember.Name, childNode.Name, StringComparison.OrdinalIgnoreCase))
                {
                    property.SetValue(model, ConvertValue.ConvertTo(property.PropertyType, childNode.InnerText));
                    break;
                }
            }
        }

        return model;
    }
}