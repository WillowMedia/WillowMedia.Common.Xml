using WillowMedia.Common.IO;
using Xunit;

namespace WillowMedia.Common.Xml.Tests;


public class DeserializeReservedCharactersTests
{
    [Fact]
    public void DeserializeReservedCharactersTest()
    {
        var filepath = "./Resources/xml/invalid-deserialized.xml";
        var file = PathHelpers.FileInfo(filepath);
        Assert.True(file?.Exists);

        var xml = System.IO.File.ReadAllText(file.FullName);
        Assert.NotEmpty(xml);
        
        var xmlHelper = new XmlHelper();
        var project = xmlHelper.Deserialize<Project>(xml);
        
        Assert.Equal("wat tekst & wat rare tekens zoals < en >", project.Projectinformation.Projectproperties.Remark);
    }

    public class Project
    {
        public ProjectInformation Projectinformation { get; set; }
    }
    
    public class ProjectInformation
    {
        public Projectproperties Projectproperties { get; set; }
    }
    
    public class Projectproperties
    {
        public string Remark { get; set; }
    }
}

/*
 <Project>
    <Projectinformation>
        <Projectproperties>
            <Remark>wat tekst &amp; wat rare tekens zoals &lt; en &gt;</Remark>
        </Projectproperties>
    </Projectinformation>
</Project>

 */