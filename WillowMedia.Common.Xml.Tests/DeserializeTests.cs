using System;
using System.Collections.Generic;
using Xunit;

namespace WillowMedia.Common.Xml.Tests;

public class DeserializeTests
{
    [Fact]
    public void DeserializeValueTypesTest()
    {
        var xmlHelper = new XmlHelper();
        var model = new TypeWithValueTypes()
        {
            Id = 123,
            Child = new TypeWithValueTypes.Subclass()
            {
                Uid = Guid.NewGuid()
            }
        };

        var xml = xmlHelper.Serialize(model);
        Assert.NotNull(xml);
        Assert.Contains(model.Child.Uid.ToString(), xml);
        
        var deserializedModel = xmlHelper.Deserialize<TypeWithValueTypes>(xml);
        Assert.NotNull(deserializedModel?.Child?.Uid);
        Assert.Equal(deserializedModel.Child.Uid, model.Child.Uid);
    }
    
    [Fact]
    public void DeserializeValueTypesNullValueTest()
    {
        var xmlHelper = new XmlHelper();
        var model = new TypeWithValueTypes()
        {
            Id = 123,
            Child = new TypeWithValueTypes.Subclass()
            {
                Id = 456,
                Uid = null
            }
        };

        var xml = xmlHelper.Serialize(model);
        Assert.NotNull(xml);
        
        var deserializedModel = xmlHelper.Deserialize<TypeWithValueTypes>(xml);
        Assert.NotNull(deserializedModel?.Child?.Id);
        Assert.Null(deserializedModel?.Child?.Uid);
    }

    public class TypeWithValueTypes
    {
        public uint? Id { get; set; }

        public Subclass Child { get; set; }
        
        public class Subclass
        {
            public uint? Id { get; set; }
            public Guid? Uid { get; set; }
        }
    }

    [Fact]
    public void DeserializeCaseInsensitive()
    {
        var xmlHelper = new XmlHelper();
        var xml = 
            "<filter filterLabel=\"test\">" +
            "   <filter filterlabel=\"test\">" +
            "   </filter>" +
            "   <filter FILTERLABEL=\"test\">" +
            "   </filter>" +
            "</filter>";
        var deserializedModel = xmlHelper.Deserialize<NodeQuestionFilter>(xml);

        Assert.NotNull(deserializedModel);
        Assert.NotNull(deserializedModel.FilterLabel);
        Assert.DoesNotContain(deserializedModel.Filters, f => f.FilterLabel == null);
    }
    
    
    public class NodeQuestionFilter
    {
        [Serializer(Name = "filterLabel", CaseInsensitive = true)]
        public string FilterLabel { get; set; }

        [Serializer(Name = "filter", CaseInsensitive = true, Inline = true)]
        public List<NodeQuestionFilter> Filters { get; set; }
    }

    [Fact]
    public void DeserializeAndSerializeElementTextTest()
    {
        // Deserialize xml string
        var filename = "test.jpg";
        var xml = $"<Image Id=\"123\">{filename}</Image>";
        var xmlHelper = new XmlHelper();
        var image = xmlHelper.Deserialize<Image>(xml);
        Assert.NotNull(image?.Filename);
        Assert.Equal(image.Filename, filename);
        
        // Serialize to xml
        var xml2 = xmlHelper.Serialize(image);
        Assert.NotNull(xml2);
        Assert.Equal($"<Image Id=\"123\">{filename}</Image>\n", xml2);
        
        // Deserialize with overwrite due to elementtext=true
        var image2 = xmlHelper.Deserialize<Image>(xml2);
        Assert.NotNull(image2?.Filename);
        Assert.Equal(image2.Filename, filename);
    }

    public class Image
    {
        public uint? Id { get; set; }
        
        [Serializer(ElementText = true)]
        public string Filename { get; set; }
    }

    [Fact]
    public void DeserializeCDataTest()
    {
        var xml =
            "<xml><title><![CDATA[Wat is het effect van het gebruik van je smartphone op je nachtrust? En 16 andere vragen over slapen ]]></title><Id>1</Id></xml>";
        var xmlHelper = new XmlHelper();
        var xmlObject = xmlHelper.Deserialize<RssPart>(xml);

        Assert.NotNull(xmlObject?.Id);
        Assert.Equal(1u, xmlObject.Id);
        Assert.NotNull(xmlObject?.title);
        Assert.False(xmlObject.title.Contains("CDATA", StringComparison.InvariantCultureIgnoreCase));
    }

    public class RssPart
    {
        public uint? Id { get; set; }
        public string title { get; set; }
    }
}