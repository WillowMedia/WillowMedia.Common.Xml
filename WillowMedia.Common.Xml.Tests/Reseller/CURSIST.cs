﻿namespace WillowMedia.Common.Xml.Tests.Reseller;

public class CURSIST
{
    public int? CURSIST_ID { get; set; }
    public string CURSIST_ACHTERNAAM { get; set; }
    public string CURSIST_TUSSENVOEGSEL { get; set; }
    public string CURSIST_VOORLETTERS { get; set; }
    public string CURSIST_VOORNAMEN { get; set; }
    public string CURSIST_ROEPNAAM { get; set; }
    public string CURSIST_GEBOORTEDATUM { get; set; }
    public string CURSIST_GEBOORTEPLAATS { get; set; }
    public string CURSIST_TELEFOONDOORKIES { get; set; }
    public string CURSIST_TELEFOONMOBIEL { get; set; }
    public string CURSIST_PRIVETELEFOONNR { get; set; }
    public string CURSIST_EMAILADRES { get; set; }
    public string CURSIST_EMAIL_ZAAK { get; set; }
    public string CURSIST_ADRES { get; set; }
    public string CURSIST_POSTCODE { get; set; }
    public string CURSIST_WOONPLAATS { get; set; }
    public string CURSIST_LAND_CODE { get; set; }
    public string CURSIST_GESLACHT { get; set; }            // M of V
}