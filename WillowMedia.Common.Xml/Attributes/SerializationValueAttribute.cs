using System;

namespace WillowMedia.Common.Xml;

[AttributeUsage(AttributeTargets.Property)]
public abstract class SerializationValueAttribute : Attribute
{
    public virtual string SerializeValue(string value)
    {
        return value;
    }  
}