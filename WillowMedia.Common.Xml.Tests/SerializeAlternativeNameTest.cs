using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;
using System.Threading;
using Xunit;

namespace WillowMedia.Common.Xml.Tests;

public class SerializeAlternativeNameTest
{
    [Fact]
    public void AlternativeNameSerializationTest()
    {
        var nlNL = new CultureInfo("nl-NL");
        Thread.CurrentThread.CurrentCulture = nlNL;
        Thread.CurrentThread.CurrentUICulture = nlNL;

        var myObject = new SerializeMeRoot();
        myObject.ANumber = 123.45f;

        var xmlHelper = new XmlHelper();
        xmlHelper.Indenting = false;
        xmlHelper.PrettyOutput = false;
        var xml = xmlHelper.Serialize(myObject);

        Assert.NotNull(xml);
        Assert.NotEqual("<SerializeMeRoot><ANumber>123.45</ANumber></SerializeMeRoot>", xml);

        myObject.IgnoreProperty = new List<ObjectWithIgnoreProperty>()
        {
            new ObjectWithIgnoreProperty()
            {
                MyValueIgnoreGetter = "bla"
            }
        };
        xml = xmlHelper.Serialize(myObject);
        Assert.NotNull(xml);
        Assert.Contains("ignoreMe", xml);
    }
        
    [Fact]
    public void SerializeListsWithAltNameTests()
    {
        var xmlHelper = new XmlHelper();
        xmlHelper.Indenting = false;
        xmlHelper.PrettyOutput = false;
            
        var myObject = new SerializeMeRoot();
        myObject.Nodes = new List<ComplicatedNode>()
        {
            new ComplicatedNode()
            {
                Label = "Level 1",
                Children = new List<ComplicatedNode>()
                {
                    new ComplicatedNode()
                    {
                        Label = "Level 2",
                    }
                }
            }
        };
        var xml = xmlHelper.Serialize(myObject);
        Assert.NotNull(xml);
        Assert.True( !xml.Contains("Complicated"));
    }
        
    // [Fact]
    // public void SerializeValueAttributeTests()
    // {
    //     var myObject = new SerializeMeRoot();
    //     myObject.MyLowercaseValue = "UPPERCASE";
    //     
    //     var xmlHelper = new XmlHelper();
    //     xmlHelper.Indenting = false;
    //     xmlHelper.PrettyOutput = false;
    //     var xml = xmlHelper.Serialize(myObject);
    //
    //     Assert.NotNull(xml);
    //     Assert.Equal("<SerializeMeRoot><MyLowercaseValue>uppercase</MyLowercaseValue></SerializeMeRoot>", xml);
    // }
        
    // /// <summary>
    // /// Test if the ignoreProperty method validates if property has ignoreDataMember or serializerIgnoreGetter
    // /// attributes
    // /// </summary>
    // [Fact]
    // public void IgnoreAttributeTest()
    // {
    //     var type = typeof(ObjectWithIgnoreProperty);
    //     var xmlHelper = new XmlHelper();
    //
    //     var ignoreGetter = type.GetProperty("MyValueIgnoreGetter");
    //     Assert.NotNull(ignoreGetter);
    //     Assert.True(Serializer.IgnoreForSerialization(ignoreGetter));
    //     
    //     var ignoreDataMember = type.GetProperty("MyValueIgnoreDataMember");
    //     Assert.NotNull(ignoreDataMember);
    //     Assert.True(Serializer.IgnoreForSerialization(ignoreDataMember));
    //     
    //     var notIgnored = type.GetProperty("NotIgnored");
    //     Assert.NotNull(notIgnored);
    //     Assert.False(Serializer.IgnoreForSerialization(notIgnored));
    // }
        
    // [Fact]
    // public void RootElementNameTest()
    // {
    //     var xmlHelper = new XmlHelper()
    //     {
    //         Indenting = false,
    //         PrettyOutput = false
    //     };
    //     
    //     var model = new MyRootObject()
    //     {
    //         SomeValue = "This is myRoot"
    //     };
    //
    //     var serialized = xmlHelper.Serialize(model);
    //     
    //     Assert.NotNull(serialized);
    //     Assert.Equal("<Rooted><SomeValue>This is myRoot</SomeValue></Rooted>", serialized);
    // }
        
        
    [SerializerName("Rooted")]
    public class MyRootObject
    {
        [Serializer(Name = "theValue")]
        public string SomeValue { get; set; }
    }

    public class ObjectWithIgnoreProperty
    {
        [SerializerIgnoreGetter]
        public string MyValueIgnoreGetter { get; set; }

        [IgnoreDataMember]
        public string MyValueIgnoreDataMember { get; set; }
        
        public string NotIgnored { get; set; }
    }

    public class ComplicatedNode
    {
        public string Label { get; set; }
            
        [Serializer(Name = "node", Inline = true)]
        public List<ComplicatedNode> Children { get; set; }
    }
        
    public class SerializeMeRoot
    {
        [SerializeLowercase]
        public string MyLowercaseValue { get; set; }

        [Serializer(Name = "theNumber")]
        public float? ANumber { get; set; }
            
        [Serializer(Name = "ignoreMe")]
        public List<ObjectWithIgnoreProperty> IgnoreProperty { get; set; }
            
        [Serializer(Name = "node", Inline = true)]
        public List<ComplicatedNode> Nodes { get; set; }
    }
}