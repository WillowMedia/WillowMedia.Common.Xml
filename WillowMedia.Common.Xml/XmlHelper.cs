﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace WillowMedia.Common.Xml;

public class XmlHelper
{
    public bool CaseSensitivePropertyNameToElement { get; set; } = true;
    public bool Indenting { get; set; } = true;
    public bool PrettyOutput { get; set; } = true;
    public bool SerializerPreferAsElement { get; set; } = false;
    public bool SerializeFixAmpOnLiteralStringValuetype { get; set; } = false;
    public CultureInfo Culture { get; set; }
        
    public virtual string Serialize<T>(T obj)
        // where T : new()
    {
        return new Serializer()
            {
                PrettyOutput = this.PrettyOutput,
                Culture = this.Culture,
                SerializerPreferAsElement = this.SerializerPreferAsElement,
                SerializeFixAmpOnLiteralStringValuetype = this.SerializeFixAmpOnLiteralStringValuetype
            }
            .Serialize(obj);
    }
        
    /// <summary>
    /// Use XDocument to read xml, and deserialize to object recursively. Currently supports
    /// all primitive properties including nullables and objects which derive from list
    /// and basic ienumerable
    /// </summary>
    public T Deserialize<T>(string xml)
    {
        return (T) Deserialize(typeof(T), xml);
    }
  
    /// <summary>
    /// Use XDocument to read xml, and deserialize to object recursively. Currently supports
    /// all primitive properties including nullables and objects which derive from list
    /// and basic ienumerable
    /// </summary>
    public object Deserialize(Type objectType, string xml)
    {
        using (var reader = new StringReader(xml))
        {
            var xdoc = XDocument.Load(reader);
            var context = new DeserializeContext();

            try
            {
                context.Current = xdoc.Root;
                context.CurrentType = objectType;
                context.Namespaces = xdoc.Root.Attributes()?
                    .Where(a => a.IsNamespaceDeclaration)
                    .ToDictionary(k => k.Name.LocalName, v => v.Value);
                if ((context.HasDefaultNamespace = context.Namespaces?.ContainsKey("xmlns") == true))
                    context.DefaultNamespace = context.Namespaces["xmlns"];

                return ParseType(objectType, null, context.Current, context);
            }
            catch (Exception ex)
            {
                throw new DeserializeException(ex, context);
            }
        }
    }

    public class DeserializeException : Exception
    {
        public DeserializeException()
        {
        }
            
        public DeserializeException(Exception ex) : base(ex.Message, ex)
        {
        }
            
        public DeserializeException(Exception ex, XElement element) : base(ex.Message, ex)
        {
            this.Element = element;
        }

        public DeserializeException(Exception ex, DeserializeContext context) : base(ex.Message, ex)
        {
            this.Element = context.Current;
            this.CurrentType = context.CurrentType;
        }
            
        public DeserializeException(string msg) : base(msg)
        {
        }
            
        public DeserializeException(string msg, Exception ex) : base(msg, ex)
        {
        }

        public readonly XElement Element;
        public readonly Type CurrentType;
    }
        
    public class DeserializeContext
    {
        public XElement Current { get; set; }
        public Type CurrentType { get; set; }
            
        public Dictionary<string, string> Namespaces { get; set; }

        public bool HasDefaultNamespace { get; set; }
        public string DefaultNamespace { get; set; }
            
        // public DeserializeContext SetCurrent(XElement element)
        // {
        //     Current = element;
        //     return this;
        // }
    }
        
    public virtual object ParseType(
        Type type,
        PropertyInfo propertyInfo, 
        XElement element,
        DeserializeContext context)
    {
        if (propertyInfo != null && propertyInfo.GetCustomAttributes<IgnoreDataMemberAttribute>().Any())
            return null;

        context.Current = element;
        context.CurrentType = type;

        switch (Type.GetTypeCode(context.CurrentType = type.NormalizeFieldType()))
        {
            case TypeCode.Empty:
            case TypeCode.DBNull:
                return null;
            case TypeCode.Object:
                if (type.NormalizeFieldType().IsValueType)
                    return ConvertValue.ConvertTo(
                        type,
                        (string.IsNullOrEmpty(element.Value) ? null : element.Value),
                        this.Culture ?? CultureInfo.InvariantCulture);

                return ParseObjectXml(type, propertyInfo, element, context);
            default:
                var value = (element.Nodes() is { } nodes && 
                             nodes.Where(n => n.GetType() != typeof(XText)).Count() > 0)
                    ? nodes.Select(n =>
                        n.NodeType switch
                        {
                            XmlNodeType.CDATA => (n as XCData).Value,
                            XmlNodeType.Text => (n as XText).Value,
                            _ => n.ToString()
                        }).Combine("")
                    : element.Value;

                return ConvertValue.ConvertTo(type, value, Culture ?? CultureInfo.InvariantCulture);
        }
    }

    public virtual object ParseObjectXml(
        Type type, 
        PropertyInfo propertyInfo, 
        XElement element,
        DeserializeContext context)
    {
        context.Current = element;
        context.CurrentType = type;
            
        if (type.IsEnumerable())
        {
            return ParseEnumeratorXml(type, propertyInfo, element, context);
        }
        else
        {
            return ParsePropertiesOfType(type, propertyInfo, element, context);
        }
    }
        
    public virtual object ParseEnumeratorXml(
        Type listType, 
        PropertyInfo propertyInfo, 
        XElement parent,
        DeserializeContext context)
    {
        context.Current = parent;
        context.CurrentType = listType;

        var result = Activator.CreateInstance(listType);
        var propertyElements = parent.Elements();
            
        ParseEnumerator(result, propertyInfo, propertyElements, context);

        return result;
    }
        
    public virtual object ParseEnumeratorXmlElements(
        Type listType, 
        PropertyInfo propertyInfo, 
        XElement parent,
        IEnumerable<XElement> children,
        DeserializeContext context)
    {
        context.Current = parent;
        context.CurrentType = listType;

        var result = Activator.CreateInstance(listType);
            
        ParseEnumerator(result, propertyInfo, children, context);

        return result;
    }

    public virtual void ParseEnumerator(
        object enumerator, 
        PropertyInfo propertyInfo,
        IEnumerable<XElement> enumeratorElements,
        DeserializeContext context)
    {
        Type enumeratorType = enumerator.GetType(); // elementInfo.propertyType;
            
        // Get type of list item
        var elementInfo = propertyInfo != null ? propertyInfo.ListElementInfo() : null;
        var useTypeAttribute = (SerializerUseTypeAttribute) null;
            
        // Interface with SerializerUseTypeAttribute
        if (elementInfo != null && 
            enumeratorType.IsGenericType &&
            elementInfo.PropertyType.IsInterface &&
            (useTypeAttribute = propertyInfo.GetCustomAttribute<SerializerUseTypeAttribute>(false)) != null)
        {
            ParseListWithTypeAttribute(
                enumerator, 
                enumeratorElements, 
                useTypeAttribute.DefaultType, 
                useTypeAttribute.IgnoreUndefined == true,
                context);
        }
        // Is it a list?
        else if (enumeratorType.IsGenericType &&
                 enumeratorType.GetGenericTypeDefinition() == typeof(List<>))
        {
            ParseList(
                enumerator, 
                elementInfo.OverrideType ?? elementInfo.PropertyType, 
                enumeratorElements,
                context);
        }
        else if (enumeratorType.BaseType.IsGenericType &&
                 enumeratorType.BaseType.GetGenericTypeDefinition() == typeof(List<>))
        {
            Type childType = null;

            // Get child type
            childType =
                elementInfo?.OverrideType ??
                (propertyInfo != null ? propertyInfo.GetCustomAttribute<XmlArrayItemAttribute>()?.Type : null) ??
                GetPropertyBaseTypeFirstGenericArgument(elementInfo) ??
                GetTypeBaseTypeFirstGenericArgument(enumeratorType);

            ParseList(enumerator, childType, enumeratorElements, context);
        }
        else
        {
            throw new NotImplementedException();                
        }
    }

    protected virtual Type GetTypeBaseTypeFirstGenericArgument(Type enumeratorType)
    {
        var baseType = enumeratorType?.BaseType;

        if (baseType == null)
            return null;
            
        var genericArguments = baseType.GetGenericArguments();

        return genericArguments?.Length > 0 
            ? genericArguments[0] 
            : null;
    }
        
    protected virtual Type GetPropertyBaseTypeFirstGenericArgument(PropertyElementInfo elementInfo)
    {
        var propertyType = elementInfo?.PropertyType;
        if (propertyType == null)
            return null;

        return GetTypeBaseTypeFirstGenericArgument(propertyType);
    }
        
    public const string TypeDescriptionAttribute = "DotNetType";
        
    public virtual void ParseListWithTypeAttribute(
        object enumerator,
        IEnumerable<XElement> children,
        Type defaultType,
        bool ignoreUndefined,
        DeserializeContext context) 
    {
        // TODO: add caching of types
            
        foreach (var childElement in children)
        {
            var childTypeName = childElement
                .Attributes()
                .FirstOrDefault(a => a.Name == TypeDescriptionAttribute)?
                .Value;

            var childType = 
                (!string.IsNullOrEmpty(childTypeName) ? TypeResolveHelper.Resolve(childTypeName) : null) ??
                defaultType ??
                (!ignoreUndefined 
                    ? throw new Exception($"Type {childTypeName} not found")
                    : (Type)null);

            // Skip to next if no type was found. When !ignoreUndefined a exception should have been throw
            // before getting here
            if (childType == null)
                continue;
                
            var value = ParseType(childType, null, childElement, context);
            (enumerator as IList)?.Add(value);
        }
    }
        
    public virtual void ParseList(
        object enumerator,
        Type childType,
        IEnumerable<XElement> children,
        DeserializeContext context) 
    {
        foreach (var childElement in children)
        {
            var value = ParseType(childType, null, childElement, context);
            (enumerator as IList)?.Add(value);
        }
    }

    protected virtual bool MatchElement(
        XElement e, 
        PropertyElementInfo childElementInfo, 
        SerializerAttribute serializerInfo,
        DeserializeContext context)
    {
        var caseInsensitve = serializerInfo?.CaseInsensitive ?? !CaseSensitivePropertyNameToElement;
        var requiredName = serializerInfo?.Name ?? childElementInfo.Name;
        var compare = caseInsensitve ? StringComparison.InvariantCultureIgnoreCase : StringComparison.InvariantCulture;

        var requiresNamespace = 
            serializerInfo?.Namespace != null &&
            context.Namespaces?.ContainsKey(serializerInfo.Namespace) == true
                ? context.Namespaces[serializerInfo.Namespace]
                : null;

        if (string.IsNullOrEmpty(requiresNamespace))
        {
            return 
                (string.IsNullOrEmpty(e.Name.NamespaceName) || (context.HasDefaultNamespace && e.Name.NamespaceName == context.DefaultNamespace)) &&
                string.Equals(e.Name.LocalName, requiredName, compare);
        }
        else
        {
            return 
                string.Equals(e.Name.NamespaceName, requiresNamespace, compare) &&
                string.Equals(e.Name.LocalName, requiredName, compare);
        }
    }

    public object ParsePropertiesOfType(
        Type objectType, 
        PropertyInfo propertyInfo,
        XElement element,
        DeserializeContext context)
    {
        if (propertyInfo != null && propertyInfo.GetCustomAttributes<IgnoreDataMemberAttribute>().Any())
            return null;
            
        if (propertyInfo != null)
        {
            var elementInfo = propertyInfo.ElementInfo();
            objectType = elementInfo.OverrideType ?? objectType;
        }

        object parent = Activator.CreateInstance(objectType);

        if (parent.GetType().IsEnumerable())
            throw new Exception("failed");

        foreach (var childPropertyInfo in objectType.GetAllProperties()
                     .Where(p =>
                         p.GetCustomAttributes(typeof(XmlIgnoreAttribute), false).Length == 0 &&
                         (p.CanWrite && p.GetSetMethod(/*nonPublic*/ true).IsPublic))
                     .OrderBy(p => p.GetCustomAttribute<SerializerAttribute>()?.ElementText == true ? 2 : 1))
        {
            if (childPropertyInfo.GetCustomAttributes<IgnoreDataMemberAttribute>().Any())
                continue;
                
            // Get the matching elements
            var isEnumerable = childPropertyInfo.PropertyType.IsEnumerable();
            var childElementInfo = childPropertyInfo.ElementInfo();
            var serializerAttr = childPropertyInfo.GetCustomAttribute<SerializerAttribute>();
                
            // Find xml element that matches this property
            var childElements = element.Elements()
                .Where(e => MatchElement(e, childElementInfo, serializerAttr, context))
                .ToList();
                
            object value = null;

            if (childElements.Count == 0)
            {
                // Check if there is a matching attribute
                var attributeName = serializerAttr?.Name ?? childElementInfo.Name ?? childElementInfo.Name;
                var caseInsensitve = serializerAttr?.CaseInsensitive ?? !CaseSensitivePropertyNameToElement;
                var compare = caseInsensitve ? StringComparison.InvariantCultureIgnoreCase : StringComparison.InvariantCulture;
                    
                // TODO: The value in an attribute can only be a primitive. This should convert to the value, but not the list
                if (element.HasAttributes && 
                    element.Attributes().FirstOrDefault(a => string.Equals(a.Name.ToString(), attributeName, compare)) is {} attr)
                {
                    // Get generic type of list and convert value
                    if (isEnumerable && childPropertyInfo.PropertyType.IsGenericType &&
                        childPropertyInfo.PropertyType.GetGenericArguments().FirstOrDefault() is {} enumeratorType &&
                        ConvertValue.ConvertTo(enumeratorType, attr.Value, Culture ?? CultureInfo.InvariantCulture) is {} enumeratorValue)
                    {
                        value ??= Activator.CreateInstance(childPropertyInfo.PropertyType);
                        (value as IList)?.Add(enumeratorValue);
                    }
                    else
                    {
                        value = ConvertValue.ConvertTo(childPropertyInfo.PropertyType, attr.Value, Culture ?? CultureInfo.InvariantCulture);
                    }
                }

                if (serializerAttr?.ElementText == true)
                    // value = ConvertValue.ConvertTo(childPropertyInfo.PropertyType, element.Value, Culture ?? CultureInfo.InvariantCulture);
                    value = ParseType(childPropertyInfo.PropertyType, childPropertyInfo, element, context);
            }
            // TODO: detect:
            // a: <elements>
            //      <element/>
            //      <element/>
            // b:
            //    <elements/>
            //    <elements/>
            else if (isEnumerable)
            {
                // Detect list type and name
                var serializerUseType = childPropertyInfo?.GetCustomAttribute<SerializerUseTypeAttribute>();
                var serializerTypeToUse = 
                    (serializerUseType?.DefaultType ??
                     childElementInfo.OverrideType ?? childElementInfo.PropertyType);
                var genericTypeDefinitionName = 
                    serializerTypeToUse.IsGenericType 
                        ? serializerTypeToUse.GetGenericArguments().FirstOrDefault()?.GetNonGenericName()
                        : serializerTypeToUse.Name;

                // is this element the parent of the children of the list
                // or is the element the child
                if (childElements.Count() == 1 &&
                    childElements.Elements().Count() > 0 &&
                    childElements.Elements().Select(e => e.Name.LocalName).Distinct().Count() == 1 &&
                    childElements.Elements().Select(e => e.Name.LocalName).First().Equals(genericTypeDefinitionName,
                        StringComparison.InvariantCulture))
                {
                    value = ParseEnumeratorXml(
                        childPropertyInfo.PropertyType,        // type of list<T>
                        childPropertyInfo,                     // the property of the list
                        childElements.First(),          // the list xml element
                        context);
                }
                // Inline elements?
                else if (serializerAttr?.Inline == true)
                {
                    var childPropertyType = childPropertyInfo.PropertyType;

                    value = ParseEnumeratorXmlElements(
                        childPropertyInfo.PropertyType,
                        childPropertyInfo,
                        element,
                        childElements,
                        context);
                }
                else
                {
                    var childPropertyType = childPropertyInfo.PropertyType;

                    value = ParseEnumeratorXmlElements(
                        childPropertyInfo.PropertyType,
                        childPropertyInfo,
                        element,
                        childElements.Elements().ToList(),
                        context);
                }
            }
            else if (childElements.Count() > 1)
            {
                throw new Exception($"Multiple items found for none enumerable property in element: {element.ToString()}");
            }
            else
            {
                // Propertytype is a value (string, int...). Determine the value for this property from the xml
                value = GetValueForType(
                    childElementInfo.OverrideType ?? childPropertyInfo.PropertyType,
                    propertyInfo,
                    context,
                    childElements);
            }

            if (value != null)
                SetValue(childPropertyInfo, parent, value);
        }

        return parent;
    }

    protected virtual object GetValueForType(
        Type typeToParse,
        PropertyInfo propertyInfo,
        DeserializeContext context,
        List<XElement> childElements)
    {
        return ParseType(
            typeToParse,
            propertyInfo,
            childElements.First(),
            context);
    }
        
    void SetValue(
        PropertyInfo propertyInfo, 
        object instance,
        object value)
    {
        if (value != null)
            switch (Type.GetTypeCode(propertyInfo.PropertyType.NormalizeFieldType()))
            {
                case TypeCode.Object:
                    break;
                default:
                    var targetType = propertyInfo.PropertyType.IsNullable() 
                        ? Nullable.GetUnderlyingType(propertyInfo.PropertyType) 
                        : propertyInfo.PropertyType; 
                    value = Convert.ChangeType(value, targetType);
                    break;
            }

        propertyInfo.SetValue(instance, value, null);
    }

    public static List<string> ValidateXmlWithXSD(
        string xsdPath,
        string xml)
    {
        // "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + 
        // xml to stream
        // convert string to stream
        // validate with XSD
        var stream = new MemoryStream(Encoding.ASCII.GetBytes(xml)); 
        var xsd = xsdPath;
        var schemas = new XmlSchemaSet();  
        schemas.Add("", xsd);

        var errors = new List<string>();

        try
        {
            var xmlDoc = XDocument.Load(stream);
            xmlDoc.Validate(schemas, (o, e) =>
            {
                errors.Add(e.Message);
            });
        }
        catch (Exception ex)
        {
            errors.Add(ex.Message);
        }

        return errors;
    }
}