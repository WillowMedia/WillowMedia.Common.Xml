using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using Xunit;

namespace WillowMedia.Common.Xml.Tests;

public class UnitTest1
{
    [Fact]
    public void Deserialize()
    {
        var xmlHelper = new XmlHelper()
        {
            CaseSensitivePropertyNameToElement = false
        };
        string xml = "<?xml version='1.0'?>\n" +
                     "<c4m>\n" +
                     "\t<Details>\n" +
                     "\t\t<Info>bla</Info>\n" +
                     "\t\t<Motoren>\n" +
                     "\t\t\t<Motor>\n" +            // Motor is the list, so its inline
                     "\t\t\t\t<Id>1</Id>"+
                     "\t\t\t\t<Extra>true</Extra>\n" +
                     "\t\t\t</Motor>\n" +
                     "\t\t</Motoren>\n" +
                     "\t</Details>\n" +
                     "</c4m>";

        Project projectA = xmlHelper.Deserialize<Project>(xml);
        ProjectB projectB = xmlHelper.Deserialize<ProjectB>(xml);

        Assert.NotNull(projectA);
        Assert.NotNull(projectB);

        Assert.NotNull(projectA.Details);
        Assert.NotNull(projectB.Details);

        Assert.Equal("bla", projectA.Details.Info);
        Assert.Equal("bla", projectB.Details.Info);

        Assert.Single(projectA.Details.Motoren.Motor);
        Assert.Single(projectB.Details.Motoren.Motor);

        Assert.Equal(1, projectA.Details.Motoren.Motor.First().Id);
        Assert.Equal(1, projectB.Details.Motoren.Motor.First().Id);
        Assert.True((projectB.Details.Motoren.Motor.First() as MotorB)?.Extra);
    }


    [Fact]
    public void SerializeGuidTests()
    {
        var xmlHelper = new XmlHelper();
        var model = new GuidModel()
        {
            Label = "Test me",
            Uid = Guid.NewGuid()
        };

        var xml = xmlHelper.Serialize(model);
        Assert.NotNull(xml);

        var restoredModel = xmlHelper.Deserialize<GuidModel>(xml);
        Assert.NotNull(restoredModel);
        Assert.NotNull(model.Uid);
        Assert.Equal(model.Uid, restoredModel.Uid);
    }

    public class GuidModel
    {
        public string Label { get; set; }
            
        public Guid? Uid { get; set; }
    }
}

#region project a
public class Project
{
    [XmlElement("Details")]
    public virtual ProjectDetails Details { get; set; }
}

public class ProjectDetails
{
    public string Info { get; set; }

    [XmlArrayItem(ElementName = "Motor", Type = typeof(Motor))]
    public virtual Motoren Motoren { get; set; }
}

public class Motoren 
{
    [Serializer(Inline = true)]
    public virtual List<Motor> Motor { get; set; }
}

//public interface IMotor
//{
//    int? Id { get; set; }
//}

public class Motor // : IMotor
{
    public virtual int? Id { get; set; }

    public override string ToString()
    {
        return "Motor";
    }
}
#endregion

#region Project B
public class ProjectB : Project
{
    [SerializerType(Type = typeof(ProjectDetailsB))]
    public override ProjectDetails Details { get; set; }
}

public class ProjectDetailsB : ProjectDetails
{
    [SerializerType(Type = typeof(MotorenB))]
    public override Motoren Motoren { get; set; }
}

public class MotorenB : Motoren
{
    [SerializerEnumeratorType(Type = typeof(MotorB))]
    [Serializer(Inline = true)]
    public override List<Motor> Motor { get; set; }
}

public class MotorB : Motor
{
    public virtual bool? Extra { get; set; }

    public override string ToString()
    {
        return "MotorB";
    }
}
#endregion