using System;

namespace WillowMedia.Common.Xml;

[AttributeUsage(AttributeTargets.Property)]
public class SerializerTypeAttribute : Attribute
{
    public Type Type { get; set; }
}