using System.Globalization;
using System.Threading;
using Xunit;

namespace WillowMedia.Common.Xml.Tests;

public class DoubleSerialization
{
    [Fact]
    public void DoubleValueDeserializeTest()
    {
        // Serialize should not be culture specific
        var nlNL = new CultureInfo("nl-NL");
        Thread.CurrentThread.CurrentCulture =
            Thread.CurrentThread.CurrentUICulture =
                nlNL;
            
        var xmlHelper = new XmlHelper();

        var item = new DoubleValue()
        {
            Value = 12.34f
        };

        var xml = xmlHelper.Serialize(item);
        Assert.NotNull(xml);
        // Assert.StartsWith("<", xml);
        // Assert.Contains(" Value=\"12.34", xml);
        Assert.Equal("<DoubleValue Value=\"12.34000015258789\"/>\n", xml);
        
        var itemDeserialized = xmlHelper.Deserialize<DoubleValue>(xml);
        Assert.NotNull(itemDeserialized);
        Assert.Equal(itemDeserialized.Value, item.Value);
    }
}

public class DoubleValue
{
    public double? Value { get; set; }
}