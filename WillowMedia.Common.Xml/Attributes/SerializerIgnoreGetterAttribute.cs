using System;

namespace WillowMedia.Common.Xml;

[AttributeUsage(AttributeTargets.Property)]
public class SerializerIgnoreGetterAttribute : Attribute
{
}