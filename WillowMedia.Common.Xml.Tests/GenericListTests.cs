using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace WillowMedia.Common.Xml.Tests;

public class GenericListTests
{
    //         [SerializerUseType(DefaultType=typeof(CircuitIO))]
    // public List<I_CircuitIO> CircuitIOCollection { get; set; }

    [Fact]
    public void GenericListWithSerializerUseTypeTest()
    {
        var xmlhelper = new XmlHelper();
        var xml = 
            "<RootElement>" +
            "  <Id>1</Id>"+ 
            "  <First>" +
            "    <FirstChildElement>" +
            "      <Id>2</Id>" +
            "      <SecondChild>" +
            "        <ISecondChildElement DotNetType=\"WillowMedia.Common.Xml.Tests.SecondChildElementBase\"><Info>base</Info></ISecondChildElement>" +
            "        <ISecondChildElement DotNetType=\"WillowMedia.Common.Xml.Tests.SecondChildAlternateElementBase\"><Info>alternate</Info></ISecondChildElement>" +
            "     </SecondChild>" + 
            "   </FirstChildElement>" +
            "  </First>" +
            "</RootElement>";

        var rootElement = xmlhelper.Deserialize<RootElement>(xml);
            
        Assert.NotNull(rootElement?.First?.FirstOrDefault()?.SecondChild?.FirstOrDefault()?.Info);
    }
}


public class RootElement
{
    public uint? Id { get; set; }

    public List<FirstChildElement> First { get; set; }
}
    
public class FirstChildElement
{
    public uint? Id { get; set; }
       
    [SerializerUseType(DefaultType=typeof(SecondChildElementBase))]
    public List<ISecondChildElement> SecondChild { get; set; }
}

public interface ISecondChildElement
{
    string Info { get; }
}
    
public class SecondChildElementBase : ISecondChildElement
{
    public virtual string Info { get; set; }
}
    
public class SecondChildAlternateElementBase : SecondChildElementBase
{
    public override string Info { get; set; }
}