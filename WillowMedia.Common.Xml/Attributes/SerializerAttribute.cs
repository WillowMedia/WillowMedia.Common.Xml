using System;

namespace WillowMedia.Common.Xml;

[AttributeUsage(AttributeTargets.Property | AttributeTargets.Class)]
public class SerializerAttribute : Attribute
{
    public string Namespace { get; set; }
        
    public string Name { get; set; }
        
    public bool CaseInsensitive { get; set; }
        
    /// <summary>
    /// List items are being placed inline of the element
    /// </summary>
    public bool Inline { get; set; }
    
    public bool Literal { get; set; }
        
    public string Formatter { get; set; }
        
    /// <summary>
    /// Apply the element text to this property
    /// </summary>
    public bool ElementText { get; set; } = false;
        
    public bool Ignore { get; set; }

    public XmlSerializing Serializer { get; set; } = XmlSerializing.AttributeOrElement;
}

public enum XmlSerializing
{
    AttributeOrElement,
    Attribute,
    Element
}