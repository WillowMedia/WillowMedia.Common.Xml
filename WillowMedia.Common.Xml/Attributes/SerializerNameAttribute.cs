﻿using System;

namespace WillowMedia.Common.Xml;

public class SerializerNameAttribute : Attribute
{
    public SerializerNameAttribute() {}

    public SerializerNameAttribute(string elementname)
    {
        this.ElementName = elementname;
    }
        
    public string ElementName { get; set; }
}