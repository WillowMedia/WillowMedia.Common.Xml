namespace WillowMedia.Common.Xml;

public class SerializeLowercaseAttribute : SerializationValueAttribute
{
    public override string SerializeValue(string value)
    {
        return value != null ? value.ToLowerInvariant() : null;
    }  
}