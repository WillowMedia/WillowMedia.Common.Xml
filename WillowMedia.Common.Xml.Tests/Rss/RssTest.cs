using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Xunit;

namespace WillowMedia.Common.Xml.Tests.Rss
{
    public class RssTest
    {
        [Fact]
        public void DeserializeRssTest()
        {
            var assembly = this.GetType().Assembly;
            var resourceName = assembly.GetManifestResourceNames().FirstOrDefault(r => r.EndsWith("Rss.xml"));

            Assert.NotNull(resourceName);

            using (var stream = assembly.GetManifestResourceStream(resourceName))
            using (var reader = new StreamReader(stream))
            {
                var xml = reader.ReadToEnd();
                var xmlHelper = new XmlHelper();
                var rss = xmlHelper.Deserialize<rss>(xml);
                
                Assert.NotNull(rss);
                Assert.NotNull(rss.channel);
                Assert.NotNull(rss.channel.item);
                Assert.Single(rss.channel.item);
            }
        }
        
        [Fact]
        public void DeserializeRss2ItemsTest()
        {
            var assembly = this.GetType().Assembly;
            var resourceName = assembly.GetManifestResourceNames().FirstOrDefault(r => r.EndsWith("Rss-2items.xml"));

            Assert.NotNull(resourceName);

            using (var stream = assembly.GetManifestResourceStream(resourceName))
            using (var reader = new StreamReader(stream))
            {
                var xml = reader.ReadToEnd();
                var xmlHelper = new XmlHelper();
                var rss = xmlHelper.Deserialize<rss>(xml);
                
                Assert.NotNull(rss);
                Assert.NotNull(rss.channel);
                Assert.NotNull(rss.channel.item);
                Assert.True(rss.channel.item.Count == 2);
            }
        }
    }

#pragma warning disable CS8981 // The type name only contains lower-cased ascii characters. Such names may become reserved for the language.
    public class rss
    {
        public channel channel { get; set; }
    }

    public class channel
    {
        public string title { get; set; }
        public string description { get; set; }
        public string link { get; set; }

        // // <atom:link href="http://www.installatie360.nl/category/brand/feed/" rel="self" type="application/rss+xml" />
        // [Serializer(Namespace = "atom")]
        // public AtomLink AtomLink { get; set; }

        public DateTime? lastBuildDate { get; set; }
        public string docs { get; set; }
        public string generator { get; set; }
        public Image image { get; set; }

        [Serializer(Namespace = "sy", Name = "updatePeriod")]
        public string UpdatePeriod { get; set; }

        [Serializer(Namespace = "sy")] 
        public string updateFrequency { get; set; }

        [Serializer(Inline = true)]
        public List<rssChannelItem> item { get; set; }
    }

    public class AtomLink
    {
        public string href { get; set; }
        public string rel { get; set; }
        public string type { get; set; }
    }

    namespace WillowMedia.Insight.RssReader.RssFeed.RssFeed
    {
        public class Image
        {
            public string url { get; set; }
        }
    }
    
    public class RssCDataAttribute : Attribute { }

    public class rssChannelItem
    {
        public string title { get; set; }
        [RssCData]
        public string description { get; set; }
        public DateTime? pubDate { get; set; }
        public string guid { get; set; }
        public string link { get; set; }
        
        //<dc:creator>
        [Serializer(Namespace = "dc")]
        public string creator { get; set; }
    }
    
    public class Image
    {
        public string url { get; set; }
    }
#pragma warning restore CS8981 // The type name only contains lower-cased ascii characters. Such names may become reserved for the language.
}