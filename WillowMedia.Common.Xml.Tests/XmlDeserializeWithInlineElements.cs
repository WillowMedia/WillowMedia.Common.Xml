using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace WillowMedia.Common.Xml.Tests;

public class XmlDeserializeWithInlineElements
{
    [Fact]
    public void InlineElementTest()
    {
        var xml = "<RootNode><Content><nl>Tekst met <say-as alias=\"NEN 25 35\">NEN 2335</say-as> tekst.</nl></Content></RootNode>";
        var xmlHelper = new XmlHelper();
        var root = xmlHelper.Deserialize<RootNode>(xml);
        
        Assert.NotNull(root?.Content);
        Assert.Equal("Tekst met <say-as alias=\"NEN 25 35\">NEN 2335</say-as> tekst.", root.Content.NL);
    }
    
    [Fact]
    public void InlineElementFallbackTest()
    {
        var xml = "<RootNode><Content>Tekst met <say-as alias=\"NEN 25 35\">NEN 2335</say-as> tekst.</Content></RootNode>";
        var xmlHelper = new XmlHelper();
        var root = xmlHelper.Deserialize<RootNode>(xml);
        
        Assert.NotNull(root?.Content?.Text);
        Assert.Equal("Tekst met <say-as alias=\"NEN 25 35\">NEN 2335</say-as> tekst.", root.Content.Text);
    }
    
    [Fact]
    public void InlineElementFallbackWithOtherContentTest()
    {
        var xml = "<RootNode><Content><nl>Tekst met <say-as alias=\"NEN 25 35\">NEN 2335</say-as> tekst.</nl></Content></RootNode>";
        var xmlHelper = new XmlHelper();
        var root = xmlHelper.Deserialize<RootNode>(xml);
        
        Assert.NotNull(root?.Content?.NL);
        Assert.Equal("Tekst met <say-as alias=\"NEN 25 35\">NEN 2335</say-as> tekst.", root.Content.NL);
        Assert.NotNull(root?.Content?.Text);
        Assert.Equal("<nl>Tekst met <say-as alias=\"NEN 25 35\">NEN 2335</say-as> tekst.</nl>", root.Content.Text);
    }
    
    public class RootNode
    {
        public LanguageContent Content { get; set; }

        public class LanguageContent
        {
            [Serializer(Ignore = true)]
            public List<string> OrderOfDeserialize { get; set; }

            private void AddOrder(string field)
            {
                (OrderOfDeserialize ??= new List<string>()).Add(field);
            }
            
            private string _text { get; set; }
            private string _nl { get; set; }
            private string _en { get; set; }
            
            [Serializer(CaseInsensitive = true)]
            public bool SomeBool { get; set; }
            
            [Serializer(ElementText = true, Ignore = true)]
            public string Text
            {
                get { return _text; }
                set { 
                    _text = value;
                    AddOrder("Text");
                }
            }

            [Serializer(Name = "nl", Serializer = XmlSerializing.Element, Literal = true)]
            public string NL
            {
                get { return _nl; }
                set { 
                    _nl = value;
                    AddOrder("NL");
                }
            }

            [Serializer(Name = "en", Serializer = XmlSerializing.Element, Literal = true)]
            public string EN
            {
                get { return _en; }
                set { 
                    _en = value;
                    AddOrder("EN");
                }
            }
            
            public static implicit operator string(LanguageContent c) => c.NL ?? c.Text;
            public static explicit operator LanguageContent(string content) => new LanguageContent() { Text = content };
        }
    }
    
    [Fact]
    public void OrderOfDeserializeWithElementTextPropertyTest()
    {
        var xml = "<RootNode><Content SomeBool=\"true\"><nl>Tekst met <say-as alias=\"NEN 25 35\">NEN 2335</say-as> tekst.</nl><en>Some english</en></Content></RootNode>";
        var xmlHelper = new XmlHelper();
        var root = xmlHelper.Deserialize<RootNode>(xml);
        
        Assert.NotNull(root?.Content);
        Assert.True(root.Content.SomeBool);
        var order = root.Content.OrderOfDeserialize;
        Assert.Equal("Text", order.Last());
    }
    
        
    [Fact]
    public void SerializeWithAmpTest()
    {
        var xml = "<RootNode><Content SomeBool=\"true\"><nl>Tekst &amp; <say-as alias=\"NEN 25 35\">NEN 2335</say-as> tekst.</nl><en>Some english</en></Content></RootNode>";
        var xmlHelper = new XmlHelper()
        {
            SerializeFixAmpOnLiteralStringValuetype = true 
        };
        
        var root = xmlHelper.Deserialize<RootNode>(xml);
        Assert.NotNull(root?.Content);
        Assert.True(root.Content.SomeBool);
        Assert.Equal("Tekst & <say-as alias=\"NEN 25 35\">NEN 2335</say-as> tekst.", root.Content.NL);
        
        // Serialize
        xml = xmlHelper.Serialize(root);
        Assert.NotNull(xml);
        Assert.Contains("Tekst &amp; ", xml);
        
        // Deserialize again
        root = xmlHelper.Deserialize<RootNode>(xml);
        Assert.NotNull(root?.Content);
        Assert.True(root.Content.SomeBool);
        Assert.Equal("Tekst & <say-as alias=\"NEN 25 35\">NEN 2335</say-as> tekst.", root.Content.NL);
    }
}