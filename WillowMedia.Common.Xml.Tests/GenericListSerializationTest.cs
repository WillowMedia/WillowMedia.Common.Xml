using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace WillowMedia.Common.Xml.Tests;

public class GenericListSerializationTest
{
    [Fact]
    public void ListSerializationTest()
    {
        var project = new RefProject();
        project.Id = 123;
        project.References = new List<RefObj<RefItem>>()
        {
            new RefObj<RefItem>()
            {
                Id = 345,
                Obj = new RefItem()
                {
                    RefId = 567
                }
            }
        };
            
        var xmlHelper = new XmlHelper()
        {
            Indenting = false,
            PrettyOutput = false
        };

        var serialized = xmlHelper.Serialize(project);
            
        Assert.Equal(
            "<RefProject Id=\"123\"><References><RefObj Id=\"345\"><Obj RefId=\"567\"/></RefObj></References></RefProject>",
            serialized);

        var deserializedProject = xmlHelper.Deserialize<RefProject>(serialized);
            
        Assert.NotNull(deserializedProject);
        Assert.NotNull(project.References.First().Obj.RefId);
        Assert.Equal(
            project.References.First().Obj.RefId,
            deserializedProject.References.First().Obj.RefId);
    }
}

public class RefProject
{
    public uint? Id { get; set; }
    public List<RefObj<RefItem>> References { get; set; }   
}

public class RefItem
{
    public uint? RefId { get; set; }
}
    
public class RefObj<T> where T: class, new()
{
    public uint? Id { get; set; }
    public T Obj { get; set; }
}