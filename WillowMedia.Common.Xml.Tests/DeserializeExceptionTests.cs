using System;
using System.Collections.Generic;
using Xunit;

namespace WillowMedia.Common.Xml.Tests;

public class DeserializeExceptionTests
{
    [Fact]
    public void TraceXmlOnExceptionTest()
    {
        var xmlHelper = new XmlHelper()
        {
            CaseSensitivePropertyNameToElement = false
        };
        string xml = "<?xml version='1.0'?>\n" +
                     "<somelist>\n" +
                     "\t<BarList>\n" +
                     "\t\t<Bar><id>1</id></Bar>" +
                     "\t\t<Bar><id>2</id><value>bla</value></Bar>" +
                     "\t</BarList>\n" +
                     "</somelist>";

        bool hadException = false;
            
        try
        {
            var result = xmlHelper.Deserialize<SomeList>(xml);
        }
        catch (Exception ex)
        {
            hadException = true;
            Assert.NotNull(ex);
            Assert.IsType<XmlHelper.DeserializeException>(ex);
            Assert.IsType<NotImplementedException>(GetLastInnerException(ex.InnerException));

            var dex = ex as XmlHelper.DeserializeException;
            Assert.NotNull(dex);
            Assert.NotNull(dex.Element);
            Assert.Equal("bla", dex.Element.Value);
        }
            
        Assert.True(hadException, "Deserialize should have thrown an exception");
    }

    protected Exception GetLastInnerException(Exception ex)
    {
        while (ex.InnerException != null)
        {
            ex = ex.InnerException;
        }

        return ex;
    }
        
    public class SomeList
    {
        public List<Bar> BarList { get; set; }
    }

    public class Bar
    {
        public int? id { get; set; }

        public string value
        {
            get { return null; }
            set { throw new NotImplementedException(); }
        }
    }
}