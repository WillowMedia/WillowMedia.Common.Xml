using System.Runtime.Serialization;
using Xunit;

namespace WillowMedia.Common.Xml.Tests;

public class IgnoreDataMemberTests
{
    [Fact]
    public void Serialize()
    {
        var objectToSerialize = new MyRoot();
        objectToSerialize.Child = new MyChild();
        objectToSerialize.Child.Id = 1;
        objectToSerialize.Child.IgnoreMe = "Gone";

        var xmlHelper = new XmlHelper();

        var xml = xmlHelper.Serialize(objectToSerialize);
            
        Assert.NotNull(xml);
        Assert.DoesNotContain("IgnoreMe", xml);
    }

    [Fact]
    public void Deserialize()
    {
        string xml = "<MyRoot><Child><Id>1</Id><IgnoreMe>blala</IgnoreMe></Child></MyRoot>";
        var xmlHelper = new XmlHelper();
        var myRoot = xmlHelper.Deserialize<MyRoot>(xml);
            
        Assert.NotNull(myRoot);
        Assert.Equal(1, myRoot.Child.Id);
        Assert.Null(myRoot.Child.IgnoreMe);
    }
}

public class MyRoot
{
    public MyChild Child { get; set; }
}
    
public class MyChild
{
    public int Id { get; set; }
            
    [IgnoreDataMember]
    public string IgnoreMe { get; set; }
}