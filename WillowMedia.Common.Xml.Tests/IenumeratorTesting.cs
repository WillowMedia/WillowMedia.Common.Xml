﻿using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace WillowMedia.Common.Xml.Tests;

public class IenumeratorTesting
{
    [Fact]
    public void DeSerializeListTests()
    {
        var xmlHelper = new XmlHelper()
        {
            CaseSensitivePropertyNameToElement = false
        };
        string xml = "<?xml version='1.0'?>\n" +
                     "<somelist>\n" +
                     "\t<BarList>\n" +
                     "\t\t<Bar><id>1</id></Bar>" +
                     "\t\t<Bar><id>2</id><value>bla</value></Bar>" +
                     "\t</BarList>\n" +
                     "</somelist>";

        var result = xmlHelper.Deserialize<SomeList>(xml);

        Assert.NotNull(result);
        Assert.NotNull(result.BarList);
        Assert.NotEmpty(result.BarList);
        Assert.Equal(2, result.BarList.Count);

        var ids = result.BarList.Where(m => m.id.HasValue).Select(m => m.id);
        Assert.Contains(1, ids);
        Assert.Contains(2, ids);

        var values = result.BarList.Where(m => !string.IsNullOrEmpty(m.value)).Select(m => m.value);
        Assert.Single(values);
        Assert.Contains("bla", values);
    }
        
    /// <summary>
    /// Serializing a list should use the name of the generic type as child element name
    /// </summary>
    [Fact]
    public void SerializeMultipleTypesInListTest()
    {
        var id = 0;
        var root = new SomeList();
        root.BarList = new List<Bar>();
        root.BarList.Add(new ExtendedBar()
        {
            id = ++id,
            Used = true
        });
        root.BarList.Add(new Bar()
        {
            id = ++id,
            value = "some info"
        });
            
        var xmlHelper = new XmlHelper()
        {
            CaseSensitivePropertyNameToElement = false,
            Indenting = false,
            PrettyOutput = false
        };
            
        var xml = xmlHelper.Serialize(root);
        Assert.NotNull(xml);
        Assert.DoesNotContain("ExtendedBar", xml);
    }
}

public class SomeList
{
    public List<Bar> BarList { get; set; }
}

public class Bar
{
    public int? id { get; set; }
    public string value { get; set; }
}

public class ExtendedBar : Bar
{
    public bool? Used { get; set; }
}