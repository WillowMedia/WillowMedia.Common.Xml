using System;
using System.Collections.Generic;
using System.Globalization;
using System.Xml.Serialization;
using Xunit;

namespace WillowMedia.Common.Xml.Tests;

public class BloomvilleXmlTests
{
    [Fact]
    public void SerializeListWithNameTest()
    {
        var courseTemplates = new BloomvilleCourseTemplates();
        courseTemplates.Add(new BloomvilleCourseTemplate()
        {
            Code = "test",
            Title = "Serialize me",
            SomeDate = new DateTime(2021, 05, 04, 13, 0, 0),
            Price = 123.56f,
            Classes = new List<BloomvilleClass>()
            {
                new BloomvilleClass()
                {
                    Code = "1234",
                    Price = 1234.56f
                }
            }
        });

        var xmlHelper = new XmlHelper()
        {
            Indenting = false,
            PrettyOutput = false,
            Culture = new CultureInfo("nl-NL")
        };
        var xml = xmlHelper.Serialize(courseTemplates);

        Assert.NotNull(xml);
        Assert.Equal(
            //"<CourseTemplates><CourseTemplate SomeDate=\"2021-05-04\"><Code>test</Code><Title>Serialize me</Title><Price>123,56</Price><Classes><Class><Code>1234</Code><Price>1234,56</Price></Class></Classes></CourseTemplate></CourseTemplates>",
            "<CourseTemplates><CourseTemplate SomeDate=\"2021-05-04\" Price=\"123.56\"><Code>test</Code><Title>Serialize me</Title><Classes><Class Code=\"1234\" Price=\"1234.56\"/></Classes></CourseTemplate></CourseTemplates>",
            xml);
    }

    [SerializerName("CourseTemplates")]
    [Serializer(Name = "CourseTemplates")]
    public class BloomvilleCourseTemplates : List<BloomvilleCourseTemplate>
    {
    }

    [SerializerName("CourseTemplate")]
    public class BloomvilleCourseTemplate
    {
        [Serializer(Serializer = XmlSerializing.Element)]
        public string Code { get; set; }
        
        [Serializer(Serializer = XmlSerializing.Element)]
        public string Title { get; set; }

        [XmlAttribute]
        [Serializer(Formatter = "{0:yyyy-MM-dd}")]
        public DateTime? SomeDate { get; set; }
            
        public float? Price {get;set;}
            
        public List<BloomvilleClass> Classes { get; set; }
    }

    [SerializerName("Class")]
    public class BloomvilleClass
    {
        public string Code { get; set; }

        /// <summary>
        /// City?
        /// </summary>
        public string Location { get; set; }

        public bool? Go { get; set; }
            
        public float? Price { get; set; }
            
        [XmlAttribute]
        public string AsAttr { get; set; }
    }
}