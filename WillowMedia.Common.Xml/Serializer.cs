﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using WillowMedia.Common.Extensions;

namespace WillowMedia.Common.Xml;

public class Serializer
{
    public virtual CultureInfo Culture { get; set; }
    public virtual bool PrettyOutput { get; set; } = true;
    public virtual bool SerializerPreferAsElement { get; set; } = false;
    public virtual bool SerializeFixAmpOnLiteralStringValuetype { get; set; } = false;
        
    protected Regex rxMatchGenericTypeName = new Regex("(.*)`[0-9]+", RegexOptions.Compiled);
        
    public string Serialize(object obj)
    {
        if (obj == null)
            throw new ArgumentNullException();

        return ObjectToTag(0, null, false, null, obj);
    }

    static readonly List<Type> ignoreTypes = new List<Type>()
    {
        typeof(SerializerIgnoreGetterAttribute),
        typeof(IgnoreDataMemberAttribute)
    };

    public static bool IgnoreForSerialization(PropertyInfo propertyInfo)
    {
        return propertyInfo.GetCustomAttributes().Any(c => ignoreTypes.Contains(c.GetType())) ||
               propertyInfo.GetCustomAttribute<SerializerAttribute>()?.Ignore == true;
    }
        
    public virtual string ObjectToTag(
        int indent,
        string proposedElementName,
        bool useType,
        PropertyInfo valueProperty,
        object value)
    {
        if (value == null)
            return string.Empty;

        if (valueProperty != null && IgnoreForSerialization(valueProperty))
            return string.Empty;

        // store optional attrbute values
        var attributes = (Dictionary<string, string>) null;
            
        var elementName = 
            useType && !string.IsNullOrEmpty(proposedElementName) 
                ? proposedElementName 
                : (proposedElementName ?? 
                   (valueProperty != null ? GetElementName(valueProperty) : null) ?? GetElementName(value));
        var elementType = value.GetType();
        var typeCode = Type.GetTypeCode(elementType);
        var childrenXml = (string) null; //string.Empty;
        var literalContent = true;
        var isValueType = false;

        // Fix generic list name
        var match = rxMatchGenericTypeName.Match(elementName);
        if (match.Success)
            elementName = match.Groups[1].Value;

        var hideParentTag = false;
            
        // Handle children
        // if value is a valuetype, so a string, int etc
        if (typeCode != TypeCode.Object || elementType.IsValueType)
        {
            var serializer = valueProperty?.GetCustomAttribute<SerializerAttribute>();
                
            if (value == null)
                childrenXml = "";
            else
                childrenXml = string.Format(
                    Culture ?? CultureInfo.InvariantCulture, 
                    serializer?.Formatter ?? "{0}", 
                    value);
                
            if (valueProperty != null)
            {
                // Use custom serializing
                var attrs = valueProperty.GetCustomAttributes<SerializationValueAttribute>();
                foreach (var valueAttribute in attrs)
                    childrenXml = valueAttribute.SerializeValue(childrenXml);
            }
                
            literalContent = serializer?.Literal == true; // Encode the value
            isValueType = true;
                
            if (literalContent && this.SerializeFixAmpOnLiteralStringValuetype && typeCode == TypeCode.String)
                childrenXml = childrenXml.Replace("&", "&amp;");
        }
        else if (IsEnumerable(elementType) && !IsGenericList(elementType))
        {
            childrenXml = "<!-- Unknown enumerable type (not a generic list) -->";
        } 
        else if (IsEnumerable(elementType))
        {
            var childElementName = (string)null;
            var builder = new StringBuilder();

            hideParentTag = valueProperty?.GetCustomAttribute<SerializerAttribute>()?.Inline == true;
                
            // Get customeAttribute from property
            var useChildTypeAttribute =
                valueProperty != null &&
                valueProperty.GetCustomAttribute<SerializerUseTypeAttribute>() != null;
                
            if (valueProperty == null)
            {
                // This occurs when root element is a list<>
                var listType = (elementType.BaseType.IsGenericType &&
                                elementType.BaseType.GetGenericTypeDefinition() == typeof(List<>))
                    ? elementType.BaseType
                    : elementType;

                var childType = listType.GetGenericArguments().First();
                childElementName = 
                    GetElementNameFromAttributeOrDefault(valueProperty) ??
                    GetElementNameFromAttributeOrDefault(childType) ??
                    childType.Name;
            }
            else
            {
                var childType = valueProperty.PropertyType.GetGenericArguments().First();
                childElementName = //GetElementName(childType);
                    GetElementNameFromAttributeOrDefault(valueProperty) ??
                    GetElementNameFromAttributeOrDefault(childType) ??
                    childType.Name;
            }

            if (string.IsNullOrEmpty(childElementName))
                throw new Exception("Missing element name of child");
                
            foreach (var item in (IEnumerable) value)
            {
                var listItemXml = ObjectToTag(
                    indent, 
                    childElementName, 
                    useChildTypeAttribute,
                    null, 
                    item);
                    
                builder.Append(listItemXml);
                // AddTag(builder, indent, listItemXml);
            }

            childrenXml = builder.ToString();
        }
        else
        {
            var propertiesBuilder = new StringBuilder();
                
            // Parse properties
            foreach (var property in elementType.GetProperties().Where(p => !IgnoreForSerialization(p)))
            {
                var propertyValue = property.GetValue(value);
                var asAttribute = property.GetCustomAttribute<XmlAttributeAttribute>() != null;
                var serializer = property?.GetCustomAttribute<SerializerAttribute>();
                var elementOrAttribute = asAttribute
                    ? XmlSerializing.Attribute
                    : serializer?.Serializer;

                if (serializer?.Ignore == true)
                    continue;
                    
                if (serializer?.ElementText == true)
                {
                    isValueType = true;
                    propertiesBuilder.Append(GetPrimitiveValueAsString(serializer, propertyValue));
                }
                else if (
                    (elementOrAttribute == XmlSerializing.Attribute ||
                     (elementOrAttribute != XmlSerializing.Element &&
                      this.SerializerPreferAsElement != true)) &&
                    PropertySuitableForAttribute(property, propertyValue))
                {
                    AddPropertyValueAsAttribute(
                        (attributes ??= new Dictionary<string, string>()), property, propertyValue);
                }
                else
                {
                    propertiesBuilder.Append(
                        ObjectToTag(indent + 1, null, false, property, propertyValue));
                }
            }

            childrenXml = propertiesBuilder.ToString();
        }

        // Handle value
            
        if (useType)
        {
            (attributes ??= new Dictionary<string, string>())
                .Add(XmlHelper.TypeDescriptionAttribute, value.GetType().FullName);
        }
            
        if (hideParentTag)
        {
            return childrenXml;
        } 
        else
        {
            var tagBuilder = new StringBuilder();
            tagBuilder.Append(this.CreateTag(
                elementName,
                attributes,
                childrenXml,
                literalContent, 
                PrettyOutput  == true ? indent : null, 
                PrettyOutput  == true && !isValueType ? indent : null,
                PrettyOutput && !isValueType,
                PrettyOutput));

            return tagBuilder.ToString();
        }
    }

    public bool PropertySuitableForAttribute(PropertyInfo property, object value)
    {
        if (value == null)
            return true;
            
        var typeCode = Type.GetTypeCode(property.PropertyType.NormalizeFieldType());

        switch (typeCode)
        {
            case TypeCode.Object:
            case TypeCode.Empty:
            case TypeCode.DBNull:
                return false;
            case TypeCode.String:
                return !InvalidForAttributes.Any(c => 
                    ((string)value)?.Contains(c) == true);
            default:
                return true;
        }
    }

    public readonly List<char> InvalidForAttributes = new List<char>()
    {
        '\t', '\n', '\r'
    };

    protected virtual void AddPropertyValueAsAttribute(Dictionary<string, string> attributes, PropertyInfo property, object value)
    {
        var serializer = property.GetCustomAttribute<SerializerAttribute>();
        var elementName = GetElementName(property);
        string attributeValue = null;
            
        if (value == null) {
            attributeValue = null;
        }
        else
        {
            switch (Type.GetTypeCode(property.PropertyType.NormalizeFieldType()))
            {
                case TypeCode.Empty:
                case TypeCode.DBNull:
                    attributeValue = null;
                    break;
                case TypeCode.Object:
                    attributeValue = value.ToString();
                    break;
                default:
                    attributeValue = GetPrimitiveValueAsString(serializer, value);
                    break;
            }
        }

        if (attributeValue != null)
            attributes.Add(elementName, attributeValue);
    }

    protected virtual string GetPrimitiveValueAsString(SerializerAttribute serializer, object value)
    {
        if (!string.IsNullOrEmpty(serializer?.Formatter))
            return String.Format(Culture ?? CultureInfo.InvariantCulture, serializer.Formatter, value);
        else
            return System.FormattableString.Invariant($"{value}");
    }

    protected virtual string CreateTag(
        string elementName,
        Dictionary<string, string> attributes,
        string childrenXml,
        bool literalContent,
        int? prefixOpenTagWithTabs,
        int? prefixCloseTagWithTabs,
        bool? appendLineFeedAfterOpenTag,
        bool? appendLineFeedAfterCloseTag)
    {
        return SimpleXmlTagGenerator.Create(
            elementName, 
            attributes, 
            childrenXml, 
            literalContent, 
            null, 
            true, 
            prefixOpenTagWithTabs, 
            prefixCloseTagWithTabs, 
            appendLineFeedAfterOpenTag,
            appendLineFeedAfterCloseTag);
    }

    protected virtual string GetElementName(PropertyInfo propertyInfo)
    {
        return
            propertyInfo.GetCustomAttribute<XmlElementAttribute>()?.ElementName ??
            propertyInfo.GetCustomAttribute<SerializerNameAttribute>()?.ElementName ??
            propertyInfo.GetCustomAttribute<SerializerAttribute>()?.Name ??
            propertyInfo.Name;
    }

    protected virtual string GetElementName(object obj)
    {
        return
            obj.GetType().GetCustomAttribute<XmlElementAttribute>()?.ElementName ??
            obj.GetType().GetCustomAttribute<SerializerNameAttribute>()?.ElementName ??
            obj.GetType().GetCustomAttribute<SerializerAttribute>()?.Name ??
            obj.GetType().Name;
    }

    protected virtual string GetElementName(Type objType)
    {
        return objType.GetCustomAttribute<XmlElementAttribute>()?.ElementName ??
               objType.GetCustomAttribute<SerializerNameAttribute>()?.ElementName ??
               objType.GetCustomAttribute<SerializerAttribute>()?.Name ??
               objType.Name;
    }

    protected virtual string GetElementNameFromAttributeOrDefault(object obj)
    {
        return obj.GetType().GetCustomAttribute<XmlElementAttribute>()?.ElementName ??
               obj.GetType().GetCustomAttribute<SerializerNameAttribute>()?.ElementName ??
               obj.GetType().GetCustomAttribute<SerializerAttribute>()?.Name;
    }
        
    protected virtual string GetElementNameFromAttributeOrDefault(Type objType)
    {
        return objType.GetCustomAttribute<XmlElementAttribute>()?.ElementName ??
               objType.GetCustomAttribute<SerializerNameAttribute>()?.ElementName ??
               objType.GetCustomAttribute<SerializerAttribute>()?.Name;
    }
        
    protected virtual string GetElementNameFromAttributeOrDefault(PropertyInfo propertyInfo)
    {
        if (propertyInfo == null)
            return null;
            
        return propertyInfo.GetCustomAttribute<XmlElementAttribute>()?.ElementName ??
               propertyInfo.GetCustomAttribute<SerializerNameAttribute>()?.ElementName ??
               propertyInfo.GetCustomAttribute<SerializerAttribute>()?.Name;
    }

    // protected virtual void AddTag(StringBuilder output, int indent, string tag)
    // {
    //     if (this.PrettyOutput)
    //         output.AppendTabs(indent);
    //
    //     output.Append(tag);
    //     
    //     if (this.PrettyOutput)
    //         output.AppendLine();
    // }

    public virtual bool IsGenericList(Type type)
    {
        return (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(List<>)) ||
               (type.BaseType.IsGenericType && type.BaseType.GetGenericTypeDefinition() == typeof(List<>));
    }
        
    public virtual bool IsEnumerable(Type type) 
    {
        return 
            Type.GetTypeCode(type) == TypeCode.Object &&
            typeof(IEnumerable).IsAssignableFrom(type);
    }
}