﻿using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace WillowMedia.Common.Xml.Tests;

public class InterfaceListTesting
{
    [Fact]
    public void InterfaceListTest()
    {
        var barType = typeof(Bar).FullName;
        var bartType = typeof(Bart).FullName;
            
        var xmlHelper = new XmlHelper()
        {
            CaseSensitivePropertyNameToElement = false
        };
            
        string xml = "<?xml version='1.0'?>\n" +
                     "<somelist>\n" +
                     "\t<BarList>\n" +
                     "\t\t<Bar DotNetType=\"WillowMedia.Common.Xml.Tests.InterfaceListTesting+Bart\"><id>1</id></Bar>" +
                     "\t\t<Bar><id>2</id><value>bla</value></Bar>" +
                     "\t</BarList>\n" +
                     "</somelist>";

        var result = xmlHelper.Deserialize<SomeList>(xml);

        Assert.NotNull(result);
        Assert.NotNull(result.BarList);
        Assert.NotEmpty(result.BarList);
        Assert.Equal(2, result.BarList.Count);

        var nrOfItems = result.BarList.Count(b => b.GetType() == typeof(Bar));
        Assert.Equal(1, nrOfItems);
        nrOfItems = result.BarList.Count(b => b.GetType() == typeof(Bart)); 
        Assert.Equal(1, nrOfItems);
            
        // serialize 
        xmlHelper.PrettyOutput = false;
        var serializedXml = xmlHelper.Serialize(result);
        Assert.NotNull(serializedXml);
            
        // <SomeList><BarList><Bart DotNetType="WillowMedia.Common.Xml.Tests.InterfaceListTesting+Bart">1</Bart><Bar DotNetType="WillowMedia.Common.Xml.Tests.InterfaceListTesting+Bar">2bla</Bar></BarList></SomeList>
        Assert.Contains(
            "<SomeList><BarList><IBar id=\"1\" DotNetType=\"WillowMedia.Common.Xml.Tests.InterfaceListTesting+Bart\"/><IBar id=\"2\" value=\"bla\" DotNetType=\"WillowMedia.Common.Xml.Tests.InterfaceListTesting+Bar\"/></BarList></SomeList>",
            serializedXml);
        // Assert.Contains("IBar DotNetType=\"WillowMedia.Common.Xml.Tests.InterfaceListTesting+Bar", serializedXml);
        // Assert.Contains("<BarList>", serializedXml);
    }

    [Fact]
    public void InterfaceListWithOverrideTest()
    {
        var xmlHelper = new XmlHelper()
        {
            CaseSensitivePropertyNameToElement = false
        };
            
        string xml = "<?xml version='1.0'?>\n" +
                     "<somelist>\n" +
                     "\t<BarList>\n" +
                     "\t\t<Bar DotNetType=\"WillowMedia.Common.Xml.Tests.InterfaceListTesting+Bart\"><id>1</id></Bar>" +
                     "\t\t<Bar><id>2</id><value>bla</value></Bar>" +
                     "\t</BarList>\n" +
                     "</somelist>";
            
        var result = xmlHelper.Deserialize<SomeListOverride>(xml);
            
        Assert.NotNull(result);
        Assert.DoesNotContain(result.BarList, b => b.GetType() != typeof(Bart)); 
    }

    public class SomeList
    {
        [SerializerUseType(DefaultType = typeof(WillowMedia.Common.Xml.Tests.InterfaceListTesting.Bar))]
        public virtual List<IBar> BarList { get; set; }
    }
        
    public class SomeListOverride : SomeList
    {
        [SerializerUseType(DefaultType = typeof(WillowMedia.Common.Xml.Tests.InterfaceListTesting.Bart))]
        public override List<IBar> BarList { get; set; }
    }
    
    public interface IBar
    {
        int? id { get; set; }
    }

    public class Bar : IBar
    {
        public int? id { get; set; }
        public string value { get; set; }
    }
    
    public class Bart : IBar
    {
        public int? id { get; set; }
        public string Name { get; set; }
    }
}