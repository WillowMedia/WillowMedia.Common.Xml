using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace WillowMedia.Common.Xml.Tests;

public class DeserializeListItemAsAttributeTest
{
    [Fact]
    public void DeserializeTest()
    {
        var xml = "<Node><Child value=\"Value1\"/></Node>";
        
        var xmlHelper = new XmlHelper();
        var node = xmlHelper.Deserialize<Node>(xml);
        
        Assert.NotNull(node);
        Assert.NotNull(node?.Child.FirstOrDefault()?.Values);
    }

    public class Node
    {
        [Serializer(Inline = true)]
        public List<Node> Child { get; set; }
        
        [Serializer(Inline = true, Name="value")]
        public List<SomeValue> Values { get; set; }
        
        public enum SomeValue
        {
            Value1,
            Value2
        }
    }
}