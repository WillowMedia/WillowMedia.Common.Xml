﻿namespace WillowMedia.Common.Xml.Tests.Reseller;

public class INSCHRIJVING
{
    public int? COURSEID { get; set; }
    public string INSCHRIJVING_ID { get; set; }
    public string PRODUCTNUMMER { get; set; }
    public string PRODUCTNAAM { get; set; }
    public string EVENEMENTNUMMER { get; set; }
    public string EVENEMENTNAAM { get; set; }
    public string INSCHRIJFDATUM { get; set; }
    public string OPMERKING { get; set; }
    public string FACTUUR_NAAR { get; set; }
    public string OPDRACHTNUMMER { get; set; }

    public CURSIST CURSIST { get; set; }
    public WERKGEVER WERKGEVER { get; set; }
}